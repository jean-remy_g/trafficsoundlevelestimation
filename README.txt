﻿
trafficEstimation is a tool destinated to the estimation of the road traffic sound level based on the Non-negative Matrix Factorization environment [2,3]
This tool has been developped during the phD of Jean-Rémy Gloaguen under the direction of Jean-François Petiot, University Professor at Central School of Nantes
with the supervision of Mathieu Lagrange et Arnaud Can, both research fellows, respectively at Central School of Nantes and Ifsttar.
Details on the experiment and on the results can be found in [1].


The program has been developped with Matlab 2017b. 

In the following, the description of the settings and some suggestions to use the tool are detailed: 

First, launch 'trafficEstimation.m' in the Matlab command window.

A window opens to fill the general settings.
The user has to fill multiples fields : 
	- 'NMF functions' to fill in the folder containing the used functions.the NMF functions leading to the functions used by this tool.
	- 'audioPath' which leads to the folder where the wav files are. audioFolder accepts a folder that includes subfolders where wav files are located.
	- 'audioName' which leads to the audio files names that will be considered. This field can be filled in to pick a specific files. 
	It can also be left empty. In this case all the wav files present in the folder will be considered. 
	
If the user wants to save the results in a mat file, click 'Yes' for 'save results' field.
The path to the resut folder has then to be filled inside of which a figure and data folder will be built. If not, a folder 'results' is created in the location
where the trafficEstimation.m is.

Differents settings to compute the spectrograms ('window w', 'noverlap', 'nfft') of the audio files can be modify. 
'p0' is the reference acoustic pressure to calculated the dB SPL.
We strongly advised to let the default values . 

To calculate the sound pressure level, the user can adapt the intergration time : 
	- '' correspond to the global sound level of the scene
		- 'all' correspond to an integration on the entire signal whatever its duration
		- else it is defined according to a specific duration (60 s, 30 s, 15 s)
	- 'integration time Lp' correspond to the integration time to display the evolution of the sound level (1 s, 125 ms for instance).


The 'type' field allows the choice of the estimator.

If NMF is chosen, different fields can be filled in, in a first tim the settings related to the dictionray learning : 
	- 'dictionaryPath' must be fill in to lead to the folder containing the wav files necessary to the dictionray learning step.
	- 'K' to adjust the number of element in the dictionary
	- 'wt' to adjust the duration of the sampling window
	- 'domain' to choose  the way the frequency axis is expressed (spectra, third octave bands or mel bands)
	- 'reduce size',  to choose the way the number of obtained elements are reduced to 'K'
	- 'frequency', in the 'spectra domain', it is possible to reduce the maximum frequency to reduce the number of points in the frequency axis
	- 'number mel bands' corresponds to the number of mal bands
Then, the NMF setting can be adjust
	- 'beta', the chosen beta-divergence for NMF
	- 'iteration', the total number of iteration
	- 'nmfType', the three NMF approches developped
	- 'size Wr' corresponds to the number of elements include in the mobile part for 'semi-supervised' NMF
	- 'threshold' fixes the threshold value for 'thresholded initialized'
	- 'smoothness' weight of this constraint
	- 'sparsity' weight of this contraint

In the end, if you want to display the evolution of the global and traffic sound level Lp (based on 'integration time Lp' setting), click 'Yes' to 'Figure Lp' field
If you want to save the matrices W, H, W0 on a mat file click 'Yes' to 'save W, H, W0' field 
(be careful, depending on the number of audio files and the size of the matrices, the weight of the mat file can be important)
	The rest of the default value of the different fields can be left to peformed the optimal NMF obtained in [1].
	However, the user can change 
	To display the global and road traffic sound pressure levels of the selected files, check the Lp Figure
	To save the H, W and V matrices, check the box.

If filter is chosen as the estimator, only the cut-off frequency has to be selected.


To a direct application of this tool, the user need at least to fill in the 'NMF functions Path', 'audio Path' and 'dictionray Path'.
The rest of the setting can be let with the default value that correspond to the optimal NMF obtained in [1].

%% BIBLIOGRAPHY %%
[1]
[2] D. D. Lee, H. S. Seung, Learning the parts of objects by nonnegative matrix factorization, Nature 401 (6755) (1999) 788–791.
[3] C. Févotte, J. Idier, Algorithms for nonnegative matrix factorization with the beta-divergence, Neural Computation 23 (9)
(2011) 2421–2456.