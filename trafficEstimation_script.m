clear all
close all
dbstop if error

% fonctionne comme l'interface graphique mais sous forme de programme 
% avec acc�s direct aux fonctions
% remplir les 4 champs suivants au minimum en chaine de caract�re ou dans
% une cellule
% d�commenter/commenter les lignes dans la partie "checking the settings" selon ces
% besoins. Mettre les param�tres qu'on souhaite changer en argument d'entr�e
% dans la fonction 'checkSetting' avec le param�tre puis la valeur souhait�e,
% tapez help checkSetting pour avoir acc�s au param�tres possibles et �
% leur valeur par d�faut
% le reste se fait tout seul (si tout va bien...)

audioPath = 'F:\PC_ifsttar\Bitbucket\sound_database\ambience\alert';   
audioName = {'alert_01_traffic','alert_02_traffic','alert_03_traffic'};
dictionaryPath = 'F:\PC_ifsttar\Bitbucket\sound_database\dictionary';
functionPath = '.\functions';

addpath(genpath(functionPath));

%% checking the settings 

% param�tre par d�fault :
% setting = checkSetting(audioPath,audioName,dictionaryPath);
%
% changer quelques param�tres
setting = checkSetting(audioPath,audioName,dictionaryPath,'sampleLength','all',...
    'waitBar',1,'saveData',0,'FigureLp',0,'nmfType','supervised','numberInitialization',10);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% dictionary building %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[W,setting] = dictionaryLearning(setting);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% NMF %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[results,setting] = NMFApplication(W,setting);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% road traffic sound level %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

results = trafficSoundLevelEstimation(results,setting);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Estimation and figures %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figurePlot(results,setting);