function [results,setting] = trafficSoundLevelEstimation(results,setting)
%% [NMF] = trafficSoundLevelEstimation(NMF,setting)

if setting.waitBar == 1
    waitbar(.95,setting.f,'traffic estimation')
end

if strcmp(setting.nmfType,'thresholded initialized')
    K = setting.K;
    W0 = results{1}{1}.W0;
    threshold = setting.threshold;
    
    for ii = 1:size(results,1)
        for jj = 1:length(results{ii})
            W = results{ii}{jj}.W;
            H = results{ii}{jj}.H;
            setting.indiceCut = jj;           
            
            dist = sum(W.*W0)./(sqrt(sum(W.^2,1)).*sqrt(sum(W0.^2,1)));
            dist(isnan(dist)) = 0;
            [~, order] = sort(dist,'descend');
            
            Wn = W(:,order);
            Hn = H(order,:);
            
            dist = dist(order);
            vec = zeros(1,K);
            
            vec(dist>threshold) = 1;
            vec(dist<=threshold) = 0;
            
            Wtraffic = Wn.*repmat(vec,size(Wn,1),1);
            Htraffic = Hn;
            
            [results{ii}{jj}.LeqTraffic,results{ii}{jj}.LpTraffic,results{ii}{jj}.spectreTraffic,~] = estimationLp(Wtraffic*Htraffic,setting);
        end
    end
else
    for ii = 1:size(results,1)
        for  jj = 1:length(results{ii})
            setting.indiceCut = jj;
            [results{ii}{jj}.LeqTraffic,results{ii}{jj}.LpTraffic,results{ii}{jj}.spectreTraffic,~] = estimationLp(results{ii}{jj}.Vap,setting);
        end
    end
end

if setting.waitBar == 1
    waitbar(1,setting.f,'finished !')
end
