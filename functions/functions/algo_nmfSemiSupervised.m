function NMF = algo_nmfSemiSupervised(H,W,Y,Z,V,iteration,setting)

beta = setting.beta;
sparsity = setting.sparsity;
smoothness = setting.smoothness;
J = setting.sizeWr;
K = size(W,2);

if ~isequal(beta,0) || ~isequal(beta,1) || ~isequal(beta,2)
    if beta < 1
        gamma = 1/(2-beta);
    elseif beta >= 1 && beta <= 2
        gamma = 1;
    else
        gamma = 1/(beta-1);
    end
end

V_1 = W*H;
Vap = V_1+Y*Z;

cost = zeros(1,length(iteration));

stepIteration = setting.stepIteration:setting.stepIteration:iteration;
if stepIteration(end)~= iteration
    stepIteration = [stepIteration iteration];
end
if stepIteration(1)~=1
    stepIteration = [1 stepIteration];
end

switch beta
    case 2
        for iter = 1:iteration
            Y = Y.*(((V*Z')./(Vap*Z')).^gamma);
            Y = Y./repmat(sum(Y),size(Y,1),1);
            Vap = V_1+Y*Z;
            
            if smoothness == 0 || strcmp(smoothForm,'traffic')
                Z = Z.*(((Y'*V)./(Y'*Vap)).^gamma);
            elseif smoothness == 1 && strcmp(smoothForm,'all')
                Z_1 = [zeros(J,1) Z(:,1:end-1)];
                Z_2 = [Z(:,2:end) zeros(J,1)];
                Z_12 = [zeros(J,1) Z(:,2:end-1) zeros(J,1)];
                Z = Z.*(Y'*V-sparsity+2*smoothness.*(Z_1+Z_2))./(Y'*Vap+2*smoothness.*(Z+Z_12));
            end
            Vap = V_1+Y*Z;
            
            if smoothness == 0
                H = H.*(((W'*V-sparsity)./(W'*Vap)).^gamma);
                H(H<0)=0;
            else
                H_1 = [zeros(K,1) H(:,1:end-1)];
                H_2 = [H(:,2:end) zeros(K,1)];
                H_12 = [zeros(K,1) H(:,2:end-1) zeros(K,1)];
                H = H.*(W'*V-sparsity+2*smoothness.*(H_1+H_2))./(W'*V_1+2*smoothness.*(H+H_12));
            end
            
            V_1 = W*H;
            Vap = V_1+Y*Z;
            
            cost(iter) = betadiv(V,Vap,beta,H,sparsity,smoothness);
        end
        
    case 1
        F = size(V,1);
        N = size(H,2);
        
        for iter = 1:iteration
            Y = Y.*((((V.*(Vap).^(-1))*Z')./repmat(sum(Z,2)',F,1)).^gamma);          
            Y = Y./repmat(sum(Y), size(Y, 1), 1);
            Vap = V_1+Y*Z;
            
            if smoothness == 0 || strcmp(smoothForm,'traffic')
                Z = Z.*(((Y'*(V.*(Vap).^(-1)))./repmat(sum(Y,1)',1,N)).^gamma);
            elseif smoothness == 1 && strcmp(smoothForm,'all')
                Z_1 = [zeros(J,1) Z(:,1:end-1)];
                Z_2 = [Z(:,2:end) zeros(J,1)];
                Z_12 = [zeros(J,1) Z(:,2:end-1) zeros(J,1)];
                Z = Z.*(((Y'*(Vap.^(beta-2).*V)-sparsity+2*smoothness.*(Z_1+Z_2))./(Y'*Vap.^(0))+2*smoothness.*(Z+Z_12)).^gamma);
            end
            Vap = V_1+Y*Z;
            
            if smoothness == 0
                H = H.*(((W.'*(V.*(Vap).^(-1)))./(sparsity+repmat(sum(W,1)',1,N))).^gamma);
            else
                H_1 = [zeros(K,1) H(:,1:end-1)];
                H_2 = [H(:,2:end) zeros(K,1)];
                H_12 = [zeros(K,1) H(:,2:end-1) zeros(K,1)];
                H = H.*((W'*(V.*V_1.^(-1))+2*smoothness.*(H_1+H_2))./(sparsity+W'*V_1.^(0)+2*smoothness.*(H+H_12)));
            end
            
            V_1 = W*H;
            Vap = V_1+Y*Z;
            
            cost(iter) = betadiv(V,Vap,beta,H,sparsity,smoothness);
        end
    case 0
        for iter = 1:iteration
            Y = Y.*((((V.*Vap.^(-2))*Z')./(Vap.^(-1)*Z')).^gamma);            
            Y = Y./repmat(sum(Y), size(Y, 1), 1);
            Vap = V_1+Y*Z;
            
            if smoothness == 0 || strcmp(smoothForm,'traffic')
                Z = Z.*((Y'*(V.*(Vap).^(-2)))./(sparsity+Y'*Vap.^(-1))).^gamma;
            elseif smoothness == 1 && strcmp(smoothForm,'all')
                Z_1 = [zeros(J,1) Z(:,1:end-1)];
                Z_2 = [Z(:,2:end) zeros(J,1)];
                Z_12 = [zeros(J,1) Z(:,2:end-1) zeros(J,1)];
                Z = Z.*(((Y'*(Vap.^(-2).*V)-sparsity+2*smoothness.*(Z_1+Z_2))./(Y'*Vap.^(-1))+2*smoothness.*(Z+Z_12)).^gamma);
            end
            Vap = V_1+Y*Z;
            
            if smoothness == 0
                H = H.*(((W'*(Vap.^(-2).*V))./(sparsity+W'*Vap.^(-1))).^gamma);
            else
                H_1 = [zeros(K,1) H(:,1:end-1)];
                H_2 = [H(:,2:end) zeros(K,1)];
                H_12 = [zeros(K,1) H(:,2:end-1) zeros(K,1)];
                H = H.*((W'*(V.*V_1.^(-2))+2*smoothness.*(H_1+H_2))./(sparsity+W'*V_1.^(-1)+2*smoothness.*(H+H_12))).^(1/2);
            end
            
            V_1 = W*H;
            Vap = V_1+Y*Z;
            
            cost(iter) = betadiv(V,Vap,beta,H,sparsity,smoothness);
        end
    otherwise
        for iter = 1:iteration
            Y = Y.*((((V.*Vap.^(beta-2))*Z')./(Vap.^(beta-1)*Z')).^gamma);
            Y = Y./repmat(sum(Y), size(Y, 1), 1);
            Vap = V_1+Y*Z;
            
            Z = Z.*(((Y'*(Vap.^(beta-2).*V))./(Y'*Vap.^(beta-1))).^gamma);
            Vap = V_1+Y*Z;
            
            if smoothness == 0
                if beta < 2
                    H = H.*(((W'*(Vap.^(beta-2).*V))./(sparsity+W'*Vap.^(beta-1))).^gamma);
                else
                    H = H.*(((W'*(Vap.^(beta-2).*V)-sparsity)./(W'*Vap.^(beta-1))).^gamma);
                end
            else
                if beta < 2
                    H_1 = [zeros(K,1) H(:,1:end-1)];
                    H_2 = [H(:,2:end) zeros(K,1)];
                    H_12 = [zeros(K,1) H(:,2:end-1) zeros(K,1)];
                    H = H.*((W'*(V.*V_1.^(beta-2))+2*smoothness.*(H_1+H_2))./(sparsity+W'*V_1.^(beta-1)+2*smoothness.*(H+H_12))).^(gamma);
                else
                    H_1 = [zeros(K,1) H(:,1:end-1)];
                    H_2 = [H(:,2:end) zeros(K,1)];
                    H_12 = [zeros(K,1) H(:,2:end-1) zeros(K,1)];
                    H = H.*((W'*(V.*V_1.^(beta-2))+2*smoothness.*(H_1+H_2)-sparsity)./(W'*V_1.^(beta-1)+2*smoothness.*(H+H_12))).^(1/2);
                end
            end
            V_1 = W*H;
            Vap = V_1+Y*Z;
            
            cost(iter) = betadiv(V,Vap,beta,H,sparsity,smoothness);
        end
end

NMF.H = H;
NMF.W = W;
NMF.Z = Z;
NMF.Y = Y;
NMF.Vtot = Vap;