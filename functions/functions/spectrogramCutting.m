function [Vout,setting] = spectrogramCutting(Vin,time,setting)

%% Temporal axis %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch setting.sampleLength 
    case 'all'
        [Vout{1},setting] = convertV(Vin,setting);
        
    otherwise
        sizeCut = setting.sampleLength ;
        dt = time(1);
        if sizeCut>time(end)
            sizeCut = time(end);
            warning('duration time of signal is inferior to sizeCut variable, sizeCut updated to the duration time of the scene')
        end
        
        ii = 0;
        while ii*sizeCut < time(end)
            n = floor(sizeCut/dt);
            
            if (ii+1)*n > size(Vin,2)
                indEnd = size(Vin,2);
            else
                indEnd = (ii+1)*n;
            end
            
            [Vout{ii+1},setting] = convertV(Vin(:,ii*n+1:indEnd),setting);
            
            ii = ii+1;
        end
        
end