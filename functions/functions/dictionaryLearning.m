function [W,setting] = dictionaryLearning(setting)
%% [W,setting] = dictionaryLearning(setting)

%% waitbar
if setting.waitBar==1
    setting.f = waitbar(0,'dictionary learning','Name','traffic estimation');
    setting.f.Units = 'Points';
    setting.f.Position =  [441 295.8750 350 56.2500];
end

%% beginning of the function 
if ~strcmp(setting.type,'Low-pass frequency filter')   % cas NMF
    
    if isempty(setting.dictionaryPath)      % check la pr�sence du chemin vers dictionaryFolder
        error('dictionaryFolder field no filled')
    end 
    
    dictionaryFolder = setting.dictionaryPath;
    if ~strcmp(dictionaryFolder,filesep)
        dictionaryFolder = strcat(dictionaryFolder,filesep);
    end
    K = setting.K;
    fileNames = setting.dictionaryFileName;
    
    if strcmp(setting.formatDictionaryFile,'.wav')
        files = dir(strcat(dictionaryFolder,'*',setting.formatDictionaryFile));      % cherche les noms des audio 
        if isempty(files)
            error('no wav file in dictionary folder')
        end
        
        if isempty(fileNames) || any(strcmp(fileNames,'''')) || any(strcmp(fileNames,'""')) || any(strcmp(fileNames,'0'))
            clear fileNames
            for ii = 1:length(files)
                fileNames{ii} = files(ii).name;
            end
        else
            fileNames(end) = [];        % suppress the last ';'
            fileNames = strsplit(fileNames,';');
        end

        W = cell(1,length(fileNames));
        for ind = 1:length(fileNames)   % g�n�ration des �l�ments pour chaque audio du dictionaire
            if setting.waitBar==1
                waitbar(ind*.33/(length(fileNames)+1),setting.f,strcat('dictionary learning: audio extraction',32,'(',num2str(ind*33/(length(files)+1),'%4.2f'),32,'%)'))
            end
            [file,setting.sr] = audioread(strcat(dictionaryFolder,fileNames{ind}));
            [W{ind},frequency] = creationDictionary(file,setting);
        end
        W = cell2mat(W);
        W(W<eps) = 0;

        if setting.waitBar==1
            waitbar((ind+1)*.33/(length(fileNames)+1),setting.f,strcat('dictionary learning: dictionary clustering',32,'(',num2str((ind+1)*33/(length(files)+1),'%4.2f'),32,'%)'))
        end

        if size(W,2) > K    % v�rifie que la taille de W est compatible avec le nombre d'�l�ments souhait�
            switch setting.reduceSize       % r�duction de la taille de W � K
                case 'rand'
                    idx = randperm(size(W,2),K);
                    W = W(:,idx);

                case 'kmeans'
                    [~,C] = kmeans(W',K,'MaxIter',200);
                    W = (C./repmat(sum(C,2),1,size(C,2)))';

            end
        end
        [W,setting] = convertW(W,frequency,setting);        % lin�aire, tiers d'octave ou mel

    else
        file = load(strcat(dictionaryFolder,fileNames));
        
        if isstruct(file)
            fieldNames = fieldnames(file);
            findW = 0;
            for ii = 1:length(fieldNames)
                if strcmp(fieldNames{ii},'W')
                    findW = 1;
                    W = file.(fieldNames{ii});
                elseif isstruct(file.(fieldNames{ii}))
                    findStruct = 1;
                    nameStruct = fieldNames{ii};
                end
            end
            if findW == 0 && findStruct == 1
                newStruct = file.(nameStruct);
                fieldNames = fieldnames(file);
                for ii = 1:length(fieldNames)
                    if strcmp(fieldNames{ii},'W')
                        findW = 1;
                        W = newStruct.(fieldNames{ii});
                    elseif strcmp(fieldNames{ii},'W0')
                        findW = 1;
                        W = newStruct.(fieldNames{ii});
                    end
                end
                if findW == 0
                    error(strcat('no dictionary field find in ',setting.fileName))
                end
            end
        elseif ismatrix(file)
            W = file;
        end
        
        if size(W,2)~= K
            error('dictionary size of loaded mat file do not correspond to the input parameters')
        elseif strcmp(setting.domain,'third octave bands') && size(W,1) ~= 25
            error('dictionary size of loaded mat file do not correspond to the input parameters')
        elseif strcmp(setting.domain,'spectra') && size(W,1) ~= setting.window*setting.noverlap/100
            error('dictionary size of loaded mat file do not correspond to the input parameters')
        end
        
    end

    
    
else        % cas filtre
    W = [];
    setting.frequency = linspace(0,44100/2,setting.nfft/2+1);
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [W,frequency] = creationDictionary(file,setting)


if min(size(file)) == 2        % stereo to mono
    file = mean(file,2)';
else
    file = file';
end

[spec,time,frequency] = audio2Spectrogram(file,setting);

switch setting.wt
    case 0 % W composed of fft of sounds get from the rms of the spectrogram
        X = rms(spec,2);
        
    otherwise   % W composed of several temporal window get by the rms of the selected window

        wt_temp = setting.wt;    % size of the temporal window
        timeEnd = floor(time(end));
        if time(end) < wt_temp
            timeEnd = wt_temp;
        end
        [~,idxBeg] = min(abs(repmat((0:wt_temp:timeEnd-wt_temp)',1,length(time))-repmat(time,floor(timeEnd/wt_temp),1)),[],2);
        [~,idxEnd] = min(abs(repmat((wt_temp:wt_temp:timeEnd)',1,length(time))-repmat(time,floor(timeEnd/wt_temp),1)),[],2);
        
        if length(idxEnd) < floor(time(end)/wt_temp)
            idxBeg = [idxBeg; idxEnd(end)+1];
            idxEnd = [idxEnd; length(time)];
        end
        X = zeros(size(spec,1),length(idxEnd));
        
        for ii = 1:length(idxBeg)
            X(:,ii) = rms(spec(:,idxBeg(ii):idxEnd(ii)-1),2);
        end
end

W = X./repmat(sum(X),size(X,1),1);  % normalisation pour chaque �l�ment

end