function setting = checkSetting(audioPath,audioName,dictionaryPath,varargin)
%% setting = checkSetting(audioPath,audioName,dictionaryPath,varargin)
% mandatory settings: 
% audioPath         => mandatory setting : path to the  audio folder, can be a folder containing folders
% audioName         => mandatory setting : name of the file, can be just a string of character ('audioName') or string in a cell ({'audioName01','audioName2',...} or can be left empty ([])
% dictionaryPath    => mandatory setting : path to the dictionary folder containing the traffic audio files
%
% optional settings
% beta              => beta value for NMF : by default 2 (euclidean distance) , can be 1 (kullback-Leibler divergence) or 0 (itakura-saito divergence) for instance
% domain            => domain of frequency representation : by default third bands octave, can be 'spectra' or 'mel bands'
% figureLp          => plot the evolution of the short-time sound pressure level, by default 1 (to display), 0 to not display the figure
% cutOffFrequency   => frequency range considered, 20 kHz by default
% sampleLength  => long time integration time to express the sound level Leq, by default 'all' (integration on all the audio file no matter its length), can be express in second (60,30)
% temporalIntegrationLp => short-time integration time in second to express the short time sound level, by default 1, can be 0.125
% iteration         => number of iteration
% nfft              => number of points of the fft for spectrogram, by default 2^12
% nmfType           => type of NMF, by default 'thresholded initialized', else 'supervised' or 'semi-supervised'
% noverlap          => overlapping for spectrogram in percent, by default 50
% numberMelbands    => in mel bands, number of bands considered, by default 40
% p0                => reference sound pressure level in Pa, by default 2e-5
% reduceSize        => way the dictionary size is reduced to K elements, by default 'kmeans', else 'rand' to pick randomly the element
% resultsFolder     => path to the result folder, if not filled, is created in the current folder
% saveData          => to save the sound pressure level estimated, by default 1, else 0 (to not save them)
% saveMatrices      => to save the NMF matrices (W, W0 and H) in mat files, by default 0 (not save) else 1 (save), be careful, the matrices can be large and allocate lot of memory (> 50 Mo)
% sizeWr            => size of the mobile dictionary Wr for semi-supervised NMF, by default 2 
% smoothness        => smoothness weight to constraint H, by default 0 (no smoothness constraint)
% sparsity          => sparsity weight to constraint H, by default 0 (no sparsity constraint)
% threshold         => threshold value for thresholded initialized NMF, by default 0.35, else included between 0 and 1
% type              => way the traffic sound level is extracted, by default 'NMF', else 'filter'
% waitBar           => to display the waitbar, by default 1 (displayed) else 0 (not displayed)
% window            =>  number of points of the hanning window for spectrogram, by default 2^12
% wt                => size, in second, of the subsampling to generate the dictionary, by default 1

if nargin<3
    error('at least 3 input args : audioPath, audioName and dictionaryPath')
else

    setting.audioPath = audioPath;
    setting.audioName = audioName;
    setting.dictionaryPath = dictionaryPath;
    
    [saveResults,window,noverlap,nfft,p0,sampleLength,temporalIntegrationLp,type,...
        K,wt,domain,reduceSize,cutOffFrequency,numberMelbands,beta,iteration,numberInitialization,...
        nmfType,sizeWr,threshold,smoothness,sparsity,FigureLp,waitBar,saveData,saveMatrices,resultsFolder,...
        formatDictionaryFile,dictionaryFileName] = ...
        process_options(varargin,'saveResults',0,'window',4096,'noverlap',50,'nfft',4096,...
        'p0',2e-5,'sampleLength','all','temporalIntegrationLp',1,...
        'type','NMF','K',300,'wt',1,'domain','third octave bands','reduceSize','kmeans',...
        'cutOffFrequency',20000,'numberMelBands',40,'beta',2,'iteration',200,'numberInitialization',1,...
        'nmfType','thresholded initialized','sizeWr',2,'threshold',0.35,'smoothness',0,...
        'sparsity',0,'FigureLp',1,'waitBar',1,'saveData',1,'saveMatrices',0,'resultsFolder','',...
        'formatDictionaryFile','.wav','dictionaryFileName','0');

    setting.saveResults = saveResults;
    setting.window = window;
    setting.noverlap = noverlap;
    setting.nfft = nfft;
    setting.p0 = p0;
    setting.sampleLength = sampleLength;
    setting.intTimeLp = temporalIntegrationLp;
    setting.type = type;
    setting.K = K;
    setting.domain = domain;
    setting.reduceSize = reduceSize;
    setting.cutOffFrequency = cutOffFrequency;
    setting.numberMelbands = numberMelbands;
    setting.beta = beta;
    setting.iteration = iteration;
    setting.nmfType = nmfType;
    setting.sizeWr = sizeWr;
    setting.threshold = threshold;
    setting.smoothness = smoothness;
    setting.sparsity = sparsity;
    setting.figureLp = FigureLp;
    setting.wt = wt;
    setting.waitBar = waitBar;
    setting.saveData = saveData;
    setting.saveMatrices = saveMatrices;
    setting.resultsFolder = resultsFolder; 
    setting.numberInitialization = numberInitialization;
    setting.formatDictionaryFile = formatDictionaryFile;
    setting.dictionaryFileName = dictionaryFileName;
    
    setting = orderfields(setting);

    if ~iscell(setting.audioPath)
        setting.audioPath = {setting.audioPath};
    end
    if ~strcmp(setting.audioPath{1}(end),filesep)
        setting.audioPath{1} = [setting.audioPath{1} filesep];
    end

    if isempty(setting.resultsFolder) || isequal(setting.resultsFolder,0)
        setting.resultsFolder = [pwd filesep];
    end

    if ~iscell(setting.resultsFolder)
        setting.resultsFolder = {setting.resultsFolder};
    end
    if ~strcmp(setting.resultsFolder{1}(end),filesep)
        setting.resultsFolder{1} = [setting.resultsFolder{1} filesep];
    end

    if ~exist(strcat(setting.resultsFolder{1},'results',filesep),'dir')
        mkdir(strcat(setting.resultsFolder{1},'results',filesep));
    end

    if ~isempty(setting.audioName)
        if ~iscell(setting.audioName)
            setting.audioName = {setting.audioName};
        end
        if contains(setting.audioName{1},';')
            idx = strfind(setting.audioName{1},';');
            for ii = 1:length(idx)
                if ii == 1
                    audioName{1} = setting.audioName{1}(1:idx(ii)-1);
                else
                    audioName{ii} = setting.audioName{1}(idx(ii-1)+1:idx(ii)-1);
                end
            end
            
        end
        for ii = 1:length(audioName)
            if ~contains(audioName{ii},'.wav')
                audioName{ii} = strcat(audioName{ii},'.wav');
            end
        end
        setting.audioName = audioName;
    else
        files = dir(strcat(setting.audioPath{1},'*.wav'));
        if isempty(files)
            content = dir(strcat(setting.audioPath{1}));
            if ~isempty(content)
                ind = 1;
                for ii = 3:length(content)
                    files = dir(strcat(setting.audioPath{1},content(ii).name,filesep,'*.wav'));
                    if ~isempty(files)
                        for jj = 1:length(files)
                            setting.audioName{ind} = strcat(content(ii).name,filesep,files(jj).name);
                            ind = ind+1;
                        end
                    else
                        warning(strcat('no wav files in',32,setting.audioPath{1},content(ii).name));
                    end
                    if isempty(setting.audioName)
                        error(strcat('no wav files in the folder found'));
                    end
                end
            else
                error(strcat('no wav files in',32,setting.audioPath{1}));
            end
        else
            warning(strcat('all the files present in ',32,setting.audioPath{1},32,'are considered'))
            for ii = 1:length(files)
                setting.audioName{ii} = files(ii).name;
            end
        end
    end
    if  ~strcmp(setting.formatDictionaryFile(1),'.')
        setting.formatDictionaryFile = strcat('.',setting.formatDictionaryFile);
    end

    %% fermeture de waitbar
    F = findall(0,'type','figure','tag','TMWWaitbar');
    close(F)

end
