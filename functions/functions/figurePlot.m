function figurePlot(results,setting)

data = [];
audioFileName = cell(1,length(results));

%% TAB %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for ii = 1:length(results)
    for jj = 1:length(results{ii})
        tab = cell(size(results{ii}{jj}.LeqTraffic,2),3);
        tab{1,1} = setting.audioName{ii};
        
        tab(:,2) = cellstr(num2str(floor(results{ii}{jj}.LeqGlobal(:).*100)/100,'%4.2f'));
        tab(:,3) = cellstr(num2str(floor(results{ii}{jj}.LeqTraffic(:).*100)/100,'%4.2f'));
        
        data = [data; tab];
    end
end


f = figure('units','normalized','outerposition',[0 0 0.9 0.9]);
t = uitable(f,'FontSize',10,'fontName','Arial');
t.ColumnName = {'audioName','LeqGlobal (dB)','LeqTraffic (dB)'};
t.Data = data;
t.Position = [20 20 900 600];
t.ColumnWidth = {300 120 120};

%% SAVE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if strcmp(setting.type,'NMF')
    if strcmp(setting.nmfType,'supervised')
        abrvNmfType = 'SUP';
    elseif strcmp(setting.nmfType,'semi-supervised')
        abrvNmfType = 'SEM';
    else
        abrvNmfType = 'TI';
    end
    
    if strcmp(setting.domain,'third octave bands')
        abrvDomain = 'Third';
    elseif strcmp(setting.domain,'mel bands')
        abrvDomain = 'Mel';
    else
        abrvDomain = 'Spec';
    end
    restName = strcat(abrvNmfType,'_',abrvDomain,'_beta_',num2str(setting.beta));
else
    restName = strcat('LowPassFilter_fc_',num2str(setting.cutOffFrequency));
end

for ii = 1:length(results)
    audioName = setting.audioName{ii}(setting.audioName{ii} ~= ' ');
    audioName = audioName(1:end-4);
    audioFileName{ii} = strcat(audioName,'_',restName);
end

if setting.saveData == 1   
    setting.dataSavePath = strcat(setting.resultsFolder{1},'data',filesep);
    setting.dataFormat = 'mat';
    setting.overwrite = 1;
    
    for ii = 1:length(results)
        for jj = 1:length(results{ii})
            dataSaved.LpGlobal{jj} = results{ii}{jj}.LpGlobal;
            dataSaved.LeqGlobal{jj} = results{ii}{jj}.LeqGlobal;
            dataSaved.LpTraffic{jj} = results{ii}{jj}.LpTraffic;
            dataSaved.LeqTraffic{jj} = results{ii}{jj}.LeqTraffic;
            dataSaved.time{jj} = results{ii}{jj}.time;
            dataSaved.settings = setting;
            
            if setting.saveMatrices == 1
                dataSaved.H{jj} = results{ii}{jj}.H;
                dataSaved.W{jj} = results{ii}{jj}.W;
                if strcmp(setting.nmfType,'semi-supervised')
                    dataSaved.Y{jj} = results{ii}{jj}.Y;
                    dataSaved.Z{jj} = results{ii}{jj}.Z;
                end
            end
        end
        setting.name = audioFileName{ii};
        dataSave(setting,dataSaved)
    end
end

%% FIGURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if setting.figureLp == 1
    for ii = 1:length(results)
        setting.name = audioFileName{ii};
        if ~strcmp(setting.sampleLength,'all')
            timeBeg = (0:(length(results{ii})-1)).*setting.sampleLength;
            timeEnd = (0:(length(results{ii})-1)).*setting.sampleLength;
        else
            timeBeg = 0;
            timeEnd = length(results{ii}{1}.LpGlobal)-1;
        end
        
        
        LpGlobal = [];
        LpTraffic = [];
        timeTot = [];
        
        for jj = 1:length(results{ii})
            fullName = strcat(setting.name,'_',num2str(timeBeg(jj)),'_',num2str(timeEnd(jj)));
            
            if setting.saveData == 1
                visible = 'on';
            else
                visible = 'on';
            end
            
            LpGlobal = [LpGlobal results{ii}{jj}.LpGlobal];
            LpTraffic = [LpTraffic results{ii}{jj}.LpTraffic];
            timeTot= [timeTot results{ii}{jj}.time];
        end
        
        fig = figure('visible',visible);
        plot(timeTot,LpGlobal,'LineWidth',2), hold on
        plot(timeTot,LpTraffic,'LineWidth',2)
        fig.Children(1).FontSize = 24;
        fig.Children(1).YLim = [floor(min([LpGlobal LpTraffic])-2) ceil(max([LpGlobal LpTraffic])+2)];
        legend('Global','estimated traffic','Location','northoutside','Orientation','horizontal')
        ylabel('L_p (dB SPL)'),xlabel('time (s)'),title(strcat('time evolution - ',strrep(fullName,'_','-')))
        grid on
        
        fig.PaperPositionMode = 'auto';
        fig.PaperUnits = 'centimeters';
        fig.PaperSize = [29.7 21.0];
        fig.PaperPosition = [0 0 fig.PaperSize(1) fig.PaperSize(2)];% [left bottom width height].
        imageSavePath = strcat(setting.resultsFolder{1},'images',filesep);
        
        if setting.saveData == 1
            saveas(fig,strcat(imageSavePath,'soundLevel',fullName,'.','fig'))
            print(fig,strcat(imageSavePath,'soundLevel',fullName),strcat('-d','pdf'))
            print(fig,strcat(imageSavePath,'soundLevel',fullName),strcat('-d','png'))
        end
        
        fig = figure('visible',visible);
        for jj = 1:length(results{ii})
            time = results{ii}{jj}.time(end);
            legendString{jj} = strcat(num2str(round(time)),32,'s');
            
            semilogx(results{ii}{jj}.frequency',20*log10(results{ii}{jj}.spectreTraffic'/setting.p0),'LineWidth',2), hold on
            fig.Children(1).FontSize = 24;
            ylabel('amp. (dB SPL)'),xlabel('frequency (Hz)'),title(strcat('spectrum - ',strrep(fullName,'_','-')))
            grid on
            
        end
        legend(legendString{:},'Location','eastoutside')
        
        fig.PaperPositionMode = 'auto';
        fig.PaperUnits = 'centimeters';
        fig.PaperSize = [29.7 21.0];
        fig.PaperPosition = [0 0 fig.PaperSize(1) fig.PaperSize(2)];% [left bottom width height].
        imageSavePath = strcat(setting.resultsFolder{1},'images',filesep);
        
        if setting.saveData == 1
            saveas(fig,strcat(imageSavePath,'spectre_',fullName,'.','fig'))
            print(fig,strcat(imageSavePath,'spectre_',fullName),strcat('-d','pdf'))
            print(fig,strcat(imageSavePath,'spectre_',fullName),strcat('-d','png'))
        end
    end
end

close(setting.f)
end