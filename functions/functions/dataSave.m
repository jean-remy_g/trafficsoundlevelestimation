function dataSave(setting, data)
%% sauvegardePlot(saveProperties, OPTname)

if nargin < 1
    error('not enough input argument in sauvegardePlot, use figureOption function to generate at least default settings')
else
    if setting.saveData == 1 || strcmp(setting.saveData,'on')
        fpath = setting.dataSavePath;
        format = setting.dataFormat; 
        name = setting.name;
        
        a = dbstack;
        if isempty(name)
            name = a(end).name;
        end
        
        if setting.overwrite == 0
            files = dir(strcat(setting.dataSavePath,'*',name,'*'));
            if isempty(files)
                num = '01';
            else
                num = num2str(length(files)+1);
                if length(files) < 10
                    num = strcat('0',num);
                end
            end
            num = strcat('_',num);
        else
            num = [];
        end
        
        nameFig = strcat(name,num);
        
        saveData(fpath,nameFig,format,data)
        disp(strcat('Saving',32,fpath,nameFig,'.',format));
    end
end

end

function saveData(fpath,name,format,data)


if strcmp(format,'mat')
    name = [fpath name '.' format];
    if isstruct(data)
        save(name,'-struct','data');
    else
        save('name','data');
    end
else
    if ismatrix(data)
        print(strcat(fpath,name),strcat('-d',format))
    else
        error(strcat('data to save in ',format,' must be a matrix'))
    end
end
end