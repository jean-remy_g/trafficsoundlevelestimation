function [Y,setting] = convertV(X,setting)


switch setting.domain
    case 'spectra'
        freq = linspace(0,setting.sr/2,setting.nfft/2+1);
        [~,indLow] = min(abs(freq-63/sqrt(2^(1/3))));
        if freq(indLow)<63
            indLow = indLow+1;
        end
        [~,indHigh] = min(abs(freq-setting.cutOffFrequency));
        
        Y = X(indLow:indHigh,:);
        setting.frequency = freq(indLow:indHigh);
        
    case 'third octave bands'
        H = setting.thirdOctaveBand;      % third band octave filter
        Y = thirdOctaveBand(X,setting,H);

    case 'mel bands'
        Y = spectre2Mel(X,setting.numberMel,setting.sr);       
        
end