function [H,Fc] = thirdOctaveBandDesign(varargin)

if nargin == 0
    noverlap = 2^11;
    sr = 44100;
else
    setting = varargin{1};
    window = setting.window;
    noverlap = floor(window*setting.noverlap/100);
    sr = setting.sr;
end

Fc = [63 80 100 125 160,...
    200 250 315 400 500 630 800 1000 1250,...
    1600 2000 2500 3150 4000 5000 6300 8000 10000 12500 16000];

H = zeros(length(Fc),noverlap+1);

for ii = 1:length(Fc)
    [B,A] = oct3dsgn(Fc(ii),sr);
	[H(ii,:),~] = oct3spec(B,A,sr,Fc(ii),'ansi',noverlap+1);
end