function [Leq,Lp,spectre,time] = estimationLp(Xinput,setting)

if iscell(Xinput)
    X = Xinput{1};
else
    X = Xinput;
end
if size(X,1)==1
    X = X';
end

indiceCut = setting.indiceCut;
sr = setting.sr;
window = setting.window;
noverlap = setting.noverlap;
step = (window*(noverlap/100))/sr;

if strcmp(setting.sampleLength,'all')
    setting.sampleLength = size(X,2)*step; 
end

timeTot =  (indiceCut-1)*setting.sampleLength+(step:step:size(X,2)*step);

dt = step;

p = sqrt(sum(X.^2));

p(p<setting.p0) = setting.p0;
Leq = 20*log10(rms(p)/setting.p0);

if size(X,2)>1
    
    ii = 0;
    while (indiceCut-1)*setting.sampleLength+ii*setting.intTimeLp < timeTot(end)
        n = floor(setting.intTimeLp/dt);
        
        if (ii+1)*n > size(p,2)
            indEnd = size(p,2);
        else
            indEnd = (ii+1)*n;
        end
        
        Lp(:,ii+1) = rms(p(ii*n+1:indEnd));
        time(:,ii+1) = timeTot(indEnd);
        ii = ii+1;
    end
        
    Lp(Lp<setting.p0) = setting.p0;
    Lp = 20*log10(Lp./setting.p0);
    
    spectre = rms(X,2);
    
else
    spectre = X;
end
    


