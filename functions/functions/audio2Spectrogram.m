function [X,time,frequency] = audio2Spectrogram(x,varargin)
%% [X,Xlinear,time,XScale,frequency] = audio2Spectrogram(x,OPTsetting)

if nargin == 2
    setting = varargin{1};
    sr = setting.sr;
    nfft = setting.nfft;
    window = setting.nfft;
    noverlap = floor(window*setting.noverlap/100);
else
    sr = 44100;
    nfft = 2^12;
    window = 2^12;
    noverlap = 2^11;
end

if size(x,1)>size(x,2)
    x = x';
end

for ii = 1:size(x,1)
    [spec,frequency,time] = spectrogram(x,window,noverlap,nfft,sr);
    X = (2/sqrt(2))*abs(spec./nfft)*1.59;       % dual, valeur efficace et correction energie hamming
end
