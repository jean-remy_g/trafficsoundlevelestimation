function NMF = estimationTraffic(set,type)

generalSetting = set.previousStep;
setting.audioPath = generalSetting.edit2.String;
setting.audioName = generalSetting.edit7.String;
setting.type = generalSetting.popupmenu3.String{generalSetting.popupmenu3.Value};
setting.window = str2double(generalSetting.edit10.String);
setting.noverlap = str2double(generalSetting.edit12.String);
setting.nfft = str2double(generalSetting.edit14.String);
setting.p0 = str2double(generalSetting.edit16.String);
setting.saveData = generalSetting.radiobutton1.Value;
setting.resultsFolder = generalSetting.edit8.String;

setting.sampleLength = generalSetting.popupmenu1.String{generalSetting.popupmenu1.Value};
if ~strcmp(setting.sampleLength,'all')
    setting.sampleLength = str2double(setting.sampleLength);
end
setting.intTimeLp = str2double(generalSetting.popupmenu2.String{generalSetting.popupmenu2.Value});

if ~iscell(setting.audioPath)
    setting.audioPath = {setting.audioPath};
end
if ~strcmp(setting.audioPath{1}(end),filesep)
    setting.audioPath{1} = [setting.audioPath{1} filesep];
end

if setting.saveData == 1
    if setting.resultsFolder=="" || strcmp(setting.resultsFolder,'0') 
        resultsFolder{1} = strcat(pwd,filesep,'results',filesep);
        clear setting.resultsFolder
        setting.resultsFolder = resultsFolder;
    end

    if ~iscell(setting.resultsFolder)
        setting.resultsFolder = {setting.resultsFolder};
    end
    if ~strcmp(setting.resultsFolder{1}(end),filesep)
        setting.resultsFolder{1} = [setting.resultsFolder{1} filesep];
    end

    if ~exist(setting.resultsFolder{1},'dir')
        mkdir(setting.resultsFolder{1});
    end
    
    if ~exist(strcat(setting.resultsFolder{1},filesep,'data',filesep),'dir')
        mkdir(strcat(setting.resultsFolder{1},filesep,'data',filesep));
    end
    
    if ~exist(strcat(setting.resultsFolder{1},filesep,'images',filesep),'dir')
        mkdir(strcat(setting.resultsFolder{1},filesep,'images',filesep));
    end
    
end

if ~isempty(setting.audioName)
    if ~iscell(setting.audioName)
        setting.audioName = {setting.audioName};
    end
    if contains(setting.audioName{1},';')
        idx = strfind(setting.audioName{1},';');
        for ii = 1:length(idx)
            if ii == 1
                audioName{1} = setting.audioName{1}(1:idx(ii)-1);
            else
                audioName{ii} = setting.audioName{1}(idx(ii-1)+1:idx(ii)-1);
            end
        end
        setting.audioName = audioName;
    end
else
    files = dir(strcat(setting.audioPath{1},'*.wav'));
    if isempty(files)
        content = dir(strcat(setting.audioPath{1}));
        if ~isempty(content)
            ind = 1;
            for ii = 3:length(content)
                files = dir(strcat(setting.audioPath{1},content(ii).name,filesep,'*.wav'));
                if ~isempty(files)
                    for jj = 1:length(files)
                        setting.audioName{ind} = strcat(content(ii).name,filesep,files(jj).name);
                        ind = ind+1;
                    end
                else
                    warning(strcat('no wav files in',32,setting.audioPath{1},content(ii).name));
                end
                if isempty(setting.audioName)
                    error(strcat('no wav files in the folder found'));
                end
            end
        else
            error(strcat('no wav files in',32,setting.audioPath{1}));
        end
    else
        warning(strcat('all the files present in ',32,setting.audioPath{1},32,'are considered'))
        for ii = 1:length(files)
            setting.audioName{ii} = files(ii).name;
        end
    end
end

setting.waitBar = 1;

switch type
    case 'Low-pass frequency filter'
        setting.cutOffFrequency = str2double(set.popupmenu1.String{set.popupmenu1.Value});
        setting.domain = 'spectra';
        setting.figureLp = set.radiobutton1.Value;
        setting.numberInitialization = 1;
        setting.nmfType = '0';
        setting.saveMatrices = 0;
        
    case 'NMF'
        setting.dictionaryPath = set.edit3.String;
        
        if set.radiobutton3.Value == 1
            setting.formatDictionaryFile = '.wav';
        elseif set.radiobutton4.Value == 1
            setting.formatDictionaryFile = '.mat';
        end
        
        setting.fileNames = set.edit33.String;
        setting.K = str2double(set.popupmenu1.String{set.popupmenu1.Value});
        setting.wt = str2double(set.popupmenu2.String{set.popupmenu2.Value});
        setting.domain = set.popupmenu3.String{set.popupmenu3.Value};
        setting.reduceSize = set.popupmenu4.String{set.popupmenu4.Value};
        setting.cutOffFrequency = str2double(set.popupmenu5.String{set.popupmenu5.Value});
        setting.beta = str2double(set.edit11.String);
        setting.iteration = str2double(set.edit13.String);
        setting.stepIteration = str2double(set.edit15.String);
        setting.nmfType = set.popupmenu6.String{set.popupmenu6.Value};
        setting.sizeWr = str2double(set.edit18.String);
        setting.threshold = str2double(set.edit20.String);
        setting.smoothness = str2double(set.edit22.String);
        setting.sparsity = str2double(set.edit24.String);
        setting.figureLp = set.radiobutton1.Value;
        setting.saveMatrices = set.radiobutton2.Value;
        setting.numberInitialization = str2double(set.edit26.String);
        
        if ~strcmp(setting.domain,'spectra')
            setting.cutOffFrequency = 20000;
        end
        setting = orderfields(setting);
end
setting = orderfields(setting);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ESTIMATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% dictionary learning %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[W,setting] = dictionaryLearning(setting);

%% nmf %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[NMF,setting] = NMFApplication(W,setting);

%% road traffic sound level %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[NMF,setting] = trafficSoundLevelEstimation(NMF,setting);

%% save data and plot fig %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figurePlot(NMF,setting)


end
