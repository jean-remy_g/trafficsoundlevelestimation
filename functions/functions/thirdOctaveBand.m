function [X_third,frequency,H] = thirdOctaveBand(X,setting,varargin)

if nargin == 3
	H = varargin{1};
	frequency = [20 25 31.5 40 50 63 80 100 125 160 200 250 315 400,...
    500 630 800 1000 1250 1600 2000 2500 3150 4000 5000 6300 8000 10000 12500 16000];
else
    [H,frequency] = thirdOctaveBandDesign(setting);
end

if size(X,1) == 1 || size(X,2)==1
   binTemp = 1;
   if size(X,1)
       X = X';
   end
else
    binTemp = size(X,2);
end

if size(H,2)>length(X)
   H = H(:,1:length(X)) ;
end

H(H>1)= 1;

X_third = zeros(size(H,1),size(X,2));
for ii = 1:size(H,1)
    X_filter = X.*repmat(H(ii,:)',1,binTemp);
    X_third(ii,:) = sqrt(sum(X_filter.^2));       % somme énergétique dans la bande fréquentielle
end