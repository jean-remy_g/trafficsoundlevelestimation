function [NMF,setting] = NMFApplication(W0,setting)

if setting.waitBar == 1
    setting.f = waitbar(.33,setting.f,strcat(setting.type,32,'estimator (33 %)'));
end

NMF = cell(length(setting.audioName),1);
seed = cell(1,setting.numberInitialization);

for kkInit = 1:setting.numberInitialization
    seed{kkInit} = rng;     % graine pour les valeurs al�atoire fix�es
end

W = W0;
NMF{1}{1}.W0 = W0;

dN = 0.33/length(setting.audioName);

for iiFiles = 1:length(setting.audioName)
    audioName = strrep(setting.audioName{iiFiles},'_','-');
    
    [file,setting.sr] = audioread(strcat(setting.audioPath{1},setting.audioName{iiFiles}));
    
    file(file == 0) = eps;
    if size(file,2)==2
        file = mean(file,2);
    end
    
    if mean(file)~=0
        file = file-mean(file);
    end
    
    [V,time] = audio2Spectrogram(file',setting);
    [V,frequency] = spectrogramCutting(V,time,setting);   

    for jjCut = 1:length(V)
        
        if setting.waitBar==1
            percent = 0.33+(iiFiles-1)*dN+jjCut*(dN/length(V));
            setting.f = waitbar(round(percent*100)/100,setting.f,...
            strcat(setting.type,32,'estimator:',32,audioName,32,'(',num2str(round(percent*100),'%4.2f'),32,'%)'));
        end
        setting.indiceCut = jjCut;
        
        [LeqGlobal,LpGlobal,~,timeCut] = estimationLp(V{jjCut},setting);

        switch setting.type
            case 'Low-pass frequency filter'
                NMF{iiFiles}{jjCut}.Vap = V{jjCut};
                setting.frequency = frequency;
                
            case 'NMF'

                costFinal = cell(1,setting.numberInitialization);
                VapFinal = costFinal;
                HFinal = costFinal;
                WFinal = costFinal;

                switch setting.nmfType
                    case 'supervised'
                        for kkInit = 1:setting.numberInitialization

                            rng(seed{kkInit})
                            H = rand(setting.K,size(V{jjCut},2));
                            NMFtemp = algo_nmfSupervised(H,W,V{jjCut},setting.iteration,setting);

                            costFinal{kkInit} = NMFtemp.cost;
                            VapFinal{kkInit} =  NMFtemp.Vap;
                        end

                    case 'semi-supervised'
                        Wrand = setting.sizeWr;

                        for kkInit = 1:setting.numberInitialization
                            rng(seed{kkInit})
                            H = rand(setting.K,size(V{jjCut},2));
                            Y =  rand(size(W,1),Wrand); Z = rand(Wrand,binTemp);
                            
                            NMFtemp = algo_nmfSemiSupervised(H,W,Y,Z,V{jjCut},setting.iteration,setting);

                            costFinal{kkInit} = NMFtemp.cost;
                            VapFinal{kkInit} =  NMFtemp.Vap;
                        end

                    case 'thresholded initialized'
                        for kkInit = 1:setting.numberInitialization
                            rng(seed{kkInit})
                            H = rand(setting.K,size(V{jjCut},2));
                            NMFtemp = algo_nmfUnsupervised(H,W,V{jjCut},setting.iteration,setting);

                            costFinal{kkInit} = NMFtemp.cost;
                            VapFinal{kkInit} =  NMFtemp.Vap;
                            HFinal{kkInit} =  NMFtemp.H;
                            WFinal{kkInit} =  NMFtemp.W;
                        end
                        NMF{iiFiles}{jjCut}.H =  mean(cat(3, HFinal{:}),3);
                        NMF{iiFiles}{jjCut}.W =  mean(cat(3, WFinal{:}),3);
                end

                NMF{iiFiles}{jjCut}.cost = mean(cell2mat(costFinal'));
                NMF{iiFiles}{jjCut}.Vap = mean(cat(3, VapFinal{:}),3);
 
        end
        
        NMF{iiFiles}{jjCut}.time = timeCut;
        NMF{iiFiles}{jjCut}.frequency = setting.frequency;
        NMF{iiFiles}{jjCut}.LpGlobal = LpGlobal;
        NMF{iiFiles}{jjCut}.LeqGlobal = LeqGlobal;
    end
end
