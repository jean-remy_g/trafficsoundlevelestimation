function [X,setting] = convertW(X,frequency,setting)


switch setting.domain
    case 'spectra'
        [~,indLow] = min(abs(frequency-63/sqrt(2^(1/3))));
        if frequency(indLow)<63
            indLow = indLow+1;
        end
        [~,indHigh] = min(abs(frequency-setting.cutOffFrequency));
        
        X = X(indLow:indHigh,:);
        frequency = frequency(indLow:indHigh);
        
    case 'third octave bands'
        [X,frequency,H] = thirdOctaveBand(X,setting);
        setting.thirdOctaveBand = H;        
end

setting.frequency = frequency;